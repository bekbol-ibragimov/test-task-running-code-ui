export enum CrudEnum {
  create = 'create',
  read = 'read',
  update = 'update',
  delete = 'delete'
}
