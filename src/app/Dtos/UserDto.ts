export interface UserDto {
  id?: number;
  createdDate?: Date;
  updatedDate?: Date;
  firstName?: string;
  lastName?: string;
  email?: string;
  password?: string;
  mobileCountryCode?: string;
  mobilePhoneNumber?: string;
  isActive?: boolean;
}
