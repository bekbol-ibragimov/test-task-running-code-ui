import { NgModule } from '@angular/core';

import { UsersRoutingModule } from './users-routing.module';

import { UsersComponent } from './users.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {NzDatePickerModule} from "ng-zorro-antd/date-picker";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzInputModule} from "ng-zorro-antd/input";
import {NzSelectModule} from "ng-zorro-antd/select";
import {NzTableModule} from "ng-zorro-antd/table";
import {HttpClientJsonpModule, HttpClientModule} from "@angular/common/http";
import {UserService} from "./user.serivce";
import { UserCrudComponent } from './user-crud/user-crud.component';
import {NzModalModule} from "ng-zorro-antd/modal";
import {NzToolTipModule} from "ng-zorro-antd/tooltip";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzMessageService} from "ng-zorro-antd/message";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NzGridModule,
    NzInputModule,
    NzButtonModule,
    NzDatePickerModule,
    NzCheckboxModule,
    NzSelectModule,
    NzTableModule,
    NzModalModule,
    NzToolTipModule,
    NzIconModule,
    NzFormModule,

    UsersRoutingModule,
    HttpClientModule,
    HttpClientJsonpModule

  ],
  declarations: [UsersComponent, UserCrudComponent],
  exports: [UsersComponent],
  providers: [UserService, NzMessageService]
})
export class UsersModule { }
