import { Component, OnInit } from '@angular/core';
import {UserDto} from "../../Dtos/UserDto";
import {UserService} from "./user.serivce";
import {CrudEnum} from "../../Enums/crud.enum";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  users: UserDto[] = [];
  crudEnum = CrudEnum;

  constructor(public service: UserService) { }

  ngOnInit() {
    this.loadData();
  }

  public loadData() {
    this.service.getAll().subscribe( (data) => {
      this.users = data;
    });
  };


  showModal(crud: CrudEnum, item: UserDto){
    this.service.crud = crud;
    console.log('item', item)
    this.service.user = { ...item };
    this.service.setValidateForm();
    this.service.showUserCrud = true;
  }
}
