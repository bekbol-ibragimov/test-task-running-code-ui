import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {UserDto} from "../../../Dtos/UserDto";
import {CrudEnum} from "../../../Enums/crud.enum";
import {UserService} from "../user.serivce";
import {NzMessageService} from "ng-zorro-antd/message";



@Component({
  selector: 'app-user-crud',
  templateUrl: './user-crud.component.html',
  styleUrls: ['./user-crud.component.scss']
})
export class UserCrudComponent implements OnInit {

  @Output() refreshTable = new EventEmitter();
  crudEnum = CrudEnum;



  constructor(
              public service: UserService,
              private msg: NzMessageService,) { }

  ngOnInit(): void {

  }

  firstName?: string;
  lastName?: string;
  email?: string;
  mobileCountryCode?: string;
  mobilePhoneNumber?: string;
  isActive?: boolean;


  submitForm(): void {
    if (this.service.validateForm.valid) {
      console.log('submit', this.service.validateForm.value);
      this.save();
    } else {
      Object.values(this.service.validateForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });

    }

  }

  save() {
    let user:UserDto =  {
      id: this.service.validateForm.controls['id'].value,
      firstName: this.service.validateForm.controls['firstName'].value,
      lastName: this.service.validateForm.controls['lastName'].value,
      email: this.service.validateForm.controls['email'].value,
      password: this.service.validateForm.controls['password'].value,
      isActive: this.service.validateForm.controls['isActive'].value,
      mobileCountryCode: this.service.validateForm.controls['mobileCountryCode'].value,
      mobilePhoneNumber: this.service.validateForm.controls['mobilePhoneNumber'].value,
    };
    console.log("user", user);
    if (this.service.crud === this.crudEnum.create) {
      this.service.add(user).subscribe(() => {
        this.msg.create('success', `Added`);
        this.refreshTable.emit();
        this.service.showUserCrud = false;
      });
    } else if (this.service.crud === this.crudEnum.update) {
      this.service.edit(user).subscribe(() => {
        this.msg.create('success', `Обновлен`);
        this.refreshTable.emit();
        this.service.showUserCrud = false;
        this.service.showUserCrud = false;
      });
    }
  }

  handleCancel(): void {
    this.service.showUserCrud = false;
  }



  updateConfirmValidator(): void {
    /** wait for refresh value */
    Promise.resolve().then(() => this.service.validateForm.controls.checkPassword.updateValueAndValidity());
  }
}
