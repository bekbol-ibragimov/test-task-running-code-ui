import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {UserDto} from "../../Dtos/UserDto";
import {Observable} from "rxjs";
import {CrudEnum} from "../../Enums/crud.enum";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {phoneNumberValidator} from "../../validators/phone-validator";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  crud: CrudEnum = CrudEnum.create;
  user: UserDto = {};
  isVisible:boolean = false;
  showUserCrud: boolean = false;
  validateForm!: FormGroup;

  baseUrl = '/api/Users/';

  constructor(protected http: HttpClient,
              private fb: FormBuilder,) {
  }

  setValidateForm(){
    this.validateForm = this.fb.group({
      id                 : [ this.user.id],
      firstName          : [this.user.firstName, [Validators.required, Validators.maxLength(100)]],
      lastName           : [this.user.lastName, [Validators.required, Validators.maxLength(100)]],
      email              : [this.user.email, [Validators.email, Validators.required, Validators.maxLength(150)]],
      password           : [null, [Validators.required, Validators.maxLength(50)]],
      checkPassword      : [null, [Validators.required,  this.confirmationValidator, Validators.maxLength(50)]],
      mobileCountryCode  : [this.user.firstName, [Validators.required, Validators.maxLength(3), phoneNumberValidator]],
      mobilePhoneNumber  : [this.user.firstName, [Validators.required, Validators.maxLength(20), phoneNumberValidator]],
      isActive           : [this.user.isActive, [Validators.required]]

    });
  }

  confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { required: true };
    } else if (control.value !== this.validateForm.controls.password.value) {
      return { confirm: true, error: true };
    }
    return {};
  };

  getAll(): Observable<UserDto[]> {
    return this.http.get<UserDto[]>(this.baseUrl);
  }

  add(dto: UserDto): Observable<UserDto> {
    return this.http.post<UserDto>( this.baseUrl, dto);
  }

  edit(dto: UserDto): Observable<UserDto> {
    return this.http.put<UserDto>(this.baseUrl + dto.id, dto);
  }

  delete(id: any): Observable<any> {
    return this.http.delete(this.baseUrl + id, {});
  }
}
